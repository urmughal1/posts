## Accenture job interview task
First,Thanks for your consideration.

I always enjoy modular and object-oriented ways to approach things in programming.
If you have any Question, regarding anything please don't hesitate to contact me.
I tried not to over engineer the things, but my focus was to be able to use this on a larger scale.

a responsive simple and easy to understand solution is implemented. Although, there is always need a room of 
improvement. since i applied for junior-mid level range i really much appreciate your feedback as this 
will help me in the future task. i hope you will enjoy the task as much as i enjoyed while doing that.

NOTE: I have done both tasks Mandatory+Bounce. I hope I did not miss any detail here.
Please contact me if you need anything, or you have any questions.

## Application Architecture
Here I am describing the main views High Level(context level) we normally use in
Engineering. you can find the pictures or screenshots in the architecture repo.

This image is related to High level Overview

<p align="center">
    <img width="781" height="708" src="/Architecture/FlowDiagram.png" alt=""/>
</p>


## Requirements
* Docker 19.03.13 or above
* Docker compose 1.25.4 or above
* MakeFile
* Bash supported
* Linux (Recommended)

### Docker Container Technology
* python version 3.7 or above
* Elasticsearch 7.4.0
* Kibana 7.4.0
* pip latest 

## Get started
Let's get into it, to start lets have a look at the repo what included in it

### Structure of the Repo
1. architecture->consist of architecture images of the application
2. docker-compose.yml file -> main docker file to run
3. Makefile-> this is the control file that is controlling the whole application
4. Readme.md ->
5. demo -> sample running app screenshots
6. post-api-> Backend Api for the app
7. Post-vue-> Frontend for the app

### App in Action
NOTE: make sure your operating system support the MakeFile.
This command will list the all controlling commands.
```
make help 
```
These are the interesting command will run the application
```
make build && make start
```
```
make backend
```
```
cd post-vue
npm install
npm run serve -- --port 3001
```
### For Testing

there are some testing usecases are also available and you can test by running that command^
```
npm run test:unit 
```



#### elasticsearch available here
```
http://localhost:9200
```

#### Kibana available here
```
http://localhost:5601
```
#### Frontend available here
```
http://localhost:3001
```
#### Backend available here
```
http://localhost:5000/api/posts
```

## Troubleshooting
for troubleshooting please use this command to get the idea.
```
make help
```

## Extra Things
following are considered to be a Bons work
* Python Backend for in pure Object-Oriented way with SOLID principles 
* Elasticsearch's implementation with python integration through the API Endpoints 
* if you know how to use Kibana you can visualize the content by going to the provided IP address
* Delete and Create functionality for the posts 


## Further Improvements
This is not the end. There is always need of improvement. If I have more time than,
I would like to do the followings
* More functionality/unit test
* There I used the set timeout (in file BaseLayout)I would like to improve it(its happening because I am accessing the getter before)
* Moreover, in the Delete and Create post API call i used location.reload() which will be replaced by Object.assign or similer solution 
#### NOTE: provided above problem and solution/improvment are skipped due to time limitation  

## Expected results
Screenshots are available in ```ROOT_DIR/demo``` directory


## Task Time Analysis
1. API Review -> 10min
2. Preparing the skeleton directory -> 30min
3. Coding -> 3 hour
4. Readme -> 1 hour
5. Screenshot and demo -> 20 min
6. Architecture diagrams -> 30 min
7. Testing with bug fixing -> 1 hour

### Further More
Previously, I have done some work on DevOps, python, vue.js, PHP, Doctrine, Liquibase, Elastic Search, Mysql and etc....
1. website purely vue.js with Redux,Babel,jest .etc and live deployed work on AWS using (ECS, EC2, S3, EFS etc)for the Organization
    - https://staging.inpera.xyz
    - https://testserver.inpera.net
    - https://saas.inpera.net
