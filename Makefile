all: help build start backend frontend

help:
	@echo "Notice->Full module Developed by <Umer Mughal @c2021>"
	@echo "Notice->Code should be used for evaluating purposes"
	@echo "Notice->In case of any Query please refer to me by <urmughal1@gmail.com>"
	@echo "-----------------------------------------------------------------"
	@echo "[COMMAND]: make build [DESCRIPTION]:to build the container please run this command"
	@echo "[COMMAND]: make start [DESCRIPTION]:to start the container please run this command"
	@echo "[COMMAND]: make stop [DESCRIPTION]:to stop the container please run this command"
	@echo "[COMMAND]: make testing [DESCRIPTION]:to test the Frontend please run this command"


build:
	@echo "Building the local docker images. This might take some time..."
	@docker-compose build --parallel

start:
	@echo 'Starting all the containers...'
	@docker-compose up -d

backend:
	@pip install -r post-api/requirements.txt
	@python3 post-api/main.py

frontend:
	@cd post-vue
	@npm install
	@npm run serve -- --port 3001
	@echo 'Your Frontend => localhost:3001'
	@echo 'Your Backend => localhost:5000'

stop:
	@echo 'Stopping all the containers...'
	@docker-compose down --remove-orphans

testing:
	@echo 'Testing for the frontend'
	@echo 'Please redirect to ROOT_DIR/post-vue/'
	@echo 'npn run test:unit'
