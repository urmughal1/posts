from elasticsearch import helpers


class DataPersistence:

    def __init__(self, elasticDatabase, postApi):
        self.elasticDatabase = elasticDatabase
        self.postApi = postApi

    # persist the data into the elasticsearch
    # elasticsearch.helper function is quit handy in this situation
    def es_persist(self, data):
        helpers.bulk(self.elasticDatabase.elastic_connection(), data, index=self.elasticDatabase.ENVIRONMENT,
                     doc_type=self.elasticDatabase.DOC_TYPE)

    # its an elastic way to store the data
    def transform_data(self, doc):
        return [{
            "_op_type": 'update',
            "_index": self.elasticDatabase.ENVIRONMENT,
            "_type": self.elasticDatabase.DOC_TYPE,
            "_id": doc['id'],
            "doc": doc,
            "doc_as_upsert": True
        }]

    # storing the data into elastic search
    def store_data(self):
        posts = self.postApi.extract_api_data()
        for post in posts:
            self.es_persist(
                self.transform_data(post))

    # Load the data from the elasticsearch using date range
    # this function support 10000 results now but you can change the size variable
    def load_data(self):
        try:
            return (self
                    .elasticDatabase
                    .elastic_connection()
                    .search(index=self.elasticDatabase.ENVIRONMENT,
                            doc_type=self.elasticDatabase.DOC_TYPE, size=10000, sort=['id:desc'])
                    )['hits']['hits']

        except Exception as ex:
            print("Exception caught: End date Must be greater than the start date")
            print(ex)

    # delete the post
    def delete_data(self, postId):
        return self.elasticDatabase.elastic_connection().delete(index=self.elasticDatabase.ENVIRONMENT,
                                                                doc_type=self.elasticDatabase.DOC_TYPE, id=postId)

    # update the post
    def update_data(self, postId, data):
        return self.elasticDatabase.elastic_connection().update(index=self.elasticDatabase.ENVIRONMENT,
                                                                doc_type=self.elasticDatabase.DOC_TYPE, id=postId,
                                                                body={"doc": data})

    # create the post
    def create_data(self, data):
        return self.elasticDatabase.elastic_connection().index(index=self.elasticDatabase.ENVIRONMENT,
                                                               doc_type=self.elasticDatabase.DOC_TYPE,
                                                               body=data)
