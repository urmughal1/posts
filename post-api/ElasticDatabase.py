from config.elasticsearch_config import elasticDataBase as es
from elasticsearch import Elasticsearch


class ElasticDatabase:
    DOC_TYPE = '_doc'
    ENVIRONMENT = es['ENVIRONMENT']

    def __init__(self):
        self.__elasticURL = es['SCHEME'] + es['HOST'] + ':' + es['PORT']
        print(self.__elasticURL)

    def elastic_connection(self):
        try:
            return Elasticsearch([self.__elasticURL])
        except Exception as ex:
            print('Exception Caught: your Elasticsearch is not properly configured.')
            print(ex)
