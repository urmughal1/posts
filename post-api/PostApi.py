import requests


class PostApi:

    # extracting the data from the API
    def __init__(self):
        pass

    def extract_api_data(self):
        response = requests.get("https://jsonplaceholder.typicode.com/posts")
        if response.status_code == 200:
            return response.json()
        else:
            raise Exception
