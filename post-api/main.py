from flask import Flask
from flask import request
from flask_cors import CORS
import json
from DataPersistence import DataPersistence
from ElasticDatabase import ElasticDatabase
from PostApi import PostApi

apiRequest = PostApi()
esDatabase = ElasticDatabase()
persistData = DataPersistence(esDatabase, apiRequest)

app = Flask(__name__)
cors = CORS(app)


def load_data_into_elasticsearch():
    persistData.store_data()
    return json.dumps(persistData.load_data())


@app.route('/api/posts', methods=['GET'])
def fetch_all_posts():
    response = persistData.load_data()
    if not response:
        return load_data_into_elasticsearch()
    return json.dumps(response)


@app.route('/api/posts/<int:postId>', methods=['DELETE'])
def delete_post(postId):
    return persistData.delete_data(postId)


@app.route('/api/posts', methods=['POST'])
def create_post():
    return persistData.create_data(json.loads(request.data).get('post').get('_source'))


@app.route('/api/posts/<int:postId>', methods=['PATCH'])
def update_post(postId):
    return persistData.update_data(postId, data=json.loads(request.data).get('post').get('_source'))


if __name__ == '__main__':
    app.run()
