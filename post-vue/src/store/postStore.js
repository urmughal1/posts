import axios from "axios";
import * as types from "./mutationType"

export default {

  namespaced: true,

  /**
   * state of the store
   */
  state: {
    aPosts:[]
  },

  getters: {

    /**
     * get the all posts
     * @param state
     * @returns {[]}
     */
    getAllPosts:(state) =>{
      return state.aPosts;
    },
  },

  actions: {

    /**
     * API call to get the all posts at one time
     * @param commit
     * @returns {Promise<unknown>}
     */
    fetchPosts({commit}){

      return new Promise((resolve,reject)=>{
        axios.get("/api/posts")
        .then(oResponse=>{
          commit(types.LOAD_POSTS,oResponse.data);
          resolve(oResponse);
        })
        .catch(error=>{reject(error)})
      });
    },

    /**
     * commit to the store
     * @param commit
     * @param data
     */
    modifyStore({commit},{data}){
      commit(types.LOAD_POSTS,data);
    }
  },

  mutations: {

    /**
     *
     * @param state
     * @param aPosts
     */
    [types.LOAD_POSTS](state, aPosts) {
      state.aPosts = aPosts;
    },
  }

}