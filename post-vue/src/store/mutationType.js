/**
 * mutations constant for the store
 * @type {string}
 */
export const LOAD_POSTS = 'load_posts';
