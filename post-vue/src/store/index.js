import Vue from 'vue'
import Vuex from 'vuex';
import postStore from "./postStore";
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    postStore
  },
  plugins: [createPersistedState()]
})
