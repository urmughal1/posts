'use strict';

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

// Full config:  https://github.com/axios/axios#request-config
axios.defaults.baseURL = 'http://localhost:5000';

Vue.use(VueAxios, axios);
