import Vue from 'vue'
import VueRouter from 'vue-router'
import BaseLayout from "../components/BaseLayout";

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "BaseLayout",
    component: BaseLayout,
  },
];

const router = new VueRouter({
  routes
})

export default router
