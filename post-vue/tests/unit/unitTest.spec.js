import { shallowMount } from '@vue/test-utils'
import EditPost from '../../src/views/EditPost'
import BaseLayout from "../../src/components/BaseLayout";

describe('To check the main layout ', () => {
  it('Displaying the Heading text check =>', () => {
    const wrapper = shallowMount(BaseLayout, {
      methods:{
        fetchPosts() {return;}
      }
    })
    expect(wrapper.find('span').text()).toBe('Posts')
  })


  it('Checking Icon Tag availability in vue=>', () => {
    const wrapper = shallowMount(BaseLayout, {
      methods:{
        fetchPosts() {return;}
      }
    })
    expect(wrapper.find('icon').exists())
  })

  it('Checking the if i have any empty div left in the code =>', () => {
    const wrapper = shallowMount(BaseLayout, {
      methods:{
        fetchPosts() {return}
      }
    })
    expect(wrapper.find('div').isEmpty()).toEqual(false)
  })
})


describe('Popup Information Checking ', () => {
  it('check the delete button text=>', () => {
    const wrapper = shallowMount(EditPost, {
    })
    expect(wrapper.find('b-button').text()).toMatch('Delete Post')
  })
})

